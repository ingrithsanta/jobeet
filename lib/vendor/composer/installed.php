<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '8c9dfd1e3e6835c8d998cfac8924bac9d4c85cc6',
    'name' => 'ingrith/jobeet',
  ),
  'versions' => 
  array (
    'escapestudios/symfony2-coding-standard' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.x-dev',
      ),
      'reference' => '78e3b0b6832c88cf7c0240b4abcd61430030d8c3',
    ),
    'friendsofsymfony1/doctrine1' => 
    array (
      'pretty_version' => 'v1.3.8',
      'version' => '1.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '3ef85915e56b29475e9bff85bfc1f80d41028a7f',
    ),
    'friendsofsymfony1/symfony1' => 
    array (
      'pretty_version' => 'v1.5.13',
      'version' => '1.5.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '28fa68237e2ed4aa25eba8c453884b0a681c4ce2',
    ),
    'ingrith/jobeet' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '8c9dfd1e3e6835c8d998cfac8924bac9d4c85cc6',
    ),
    'squizlabs/php_codesniffer' => 
    array (
      'pretty_version' => '3.5.8',
      'version' => '3.5.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d583721a7157ee997f235f327de038e7ea6dac4',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v5.4.12',
      'version' => '5.4.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '181b89f18a90f8925ef805f950d47a7190e9b950',
    ),
  ),
);
